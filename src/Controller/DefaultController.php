<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use App\Form\ReactionFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index()
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findAll();

        $form = $this->createForm(ReactionFormType::class);

        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'category' => $category,
            'form' => $form->createView()
        ]);
    }
}
