<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Post;
use App\Form\ReactionFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{

    /**
     * @Route("/post/{id}/", name="post_index")
     */
    public function view($id, Request $request, Post $post)
    {
        $owner = false;

        $entityManager = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $posts = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findBy(['id' => $id]);

        $comments = $this->getDoctrine()
            ->getRepository(Comment::class)
            ->findBy(['post' => $id]);

        $form = $this->createForm(ReactionFormType::class);
            if ($request->isMethod('POST')) {
                $form->submit($request->request->get($form->getName()));

                if ($form->isSubmitted() && $form->isValid()) {
                    $comment = new Comment();
                    $comment->setUser($user);
                    $comment->setPost($post);
                    $comment->setText($form["text"]->getData());
                    $entityManager->persist($comment);
                    $entityManager->flush();

                    return $this->redirectToRoute('post_comment', [
                        'id' => $id
                    ]);
                }
            }

        return $this->render('post/show.html.twig', [
            'posts' => $posts,
            'comments' => $comments,
            'form' => $form->createView(),
            'owner' => $owner
        ]);
    }
    /**
     * @Route("/post/{id}/comment/posted/", name="post_comment")
     */
    public function comment($id){
        return $this->redirectToRoute('post_index', [
            'id' => $id
        ]);
    }
}
