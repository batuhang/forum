<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    /**
     * @Route("/profile/dashboard/posts", name="user_posts")
     */
    public function own_posts()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        $posts = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findBy(['user' => $user]);

        return $this->render('user/index.html.twig', [
            'posts' => $posts,
        ]);
    }
}
