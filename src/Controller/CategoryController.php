<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category/{id}", name="category_index")
     */
    public function view($id, Category $category)
    {
        $posts = $this->getDoctrine()
            ->getRepository(Post::class)
            ->findBy(['category' => $id]);

        return $this->render('category/index.html.twig', [
            'posts' => $posts,
        ]);
    }
}
